#! /usr/bin/env python
# -*- coding: utf-8 -*-

from random import choice


class CatchPhraseGenerator():
    def __init__(self):
        self._track_removed_phrases = [
            "Наш плейлист покидает этот трек:",
            "Прощаемся с этим треком:",
            "Было чудесно катать под этот трек, пришла пора с ним расстаться:",
            "До свидания:", ]

        self._self_voting_phrases = [
            "За свой трек голосовать нельзя, увы :(",
            "Нельзя голосовать за свой трек!",
            "У нас не принято голосовать за свой трек!",
            "Трек, возможно и крут, но все равно голосовать за свой трек нельзя!", ]

        self._double_voting_phrases = [
            "Воу, полегче! Нельзя голосовать дважды!)",
            "Стоп! Ты уже проголосовал!",
            "Трек отличный, спору нет, но голосовать дважды нельзя :(",
            "Я и с первого раза понял, голосовать повторно нельзя!", ]

        self._playlist_command_phrases = [
            "Наш плейлист:",
            "Катаем под это:",
            "Неплохая подборка!",
            "А вот и наши треки:", ]

    def get_track_removed_phrase(self):
        return choice(self._track_removed_phrases)

    def get_self_voting_phrase(self):
        return choice(self._self_voting_phrases)

    def get_double_voting_phrase(self):
        return choice(self._double_voting_phrases)

    def get_playlist_command_phrase(self):
        return choice(self._playlist_command_phrases)
