#! /usr/bin/env python
# -*- coding: utf-8 -*-


class TestConfiguration:
    def __init__(self):
        self.token = "398447028:AAEUy-p3oKN_TWh9_JcpVNuFBmEpc7t1JPg"
        self.bot_name = "@FlairfoxTest_Bot"
        self.track_expiry = 20
        self.expiry_check_interval = 60
        self.votes_to_approve = 1
        self.self_voting_enabled = True


class ReleaseConfiguration:
    def __init__(self):
        self.token = "361853891:AAHp1u62VOKgSmjplBUJvNSFeSZz7W3z_K4"
        self.bot_name = "@JohnnyTheWheels_Bot"
        self.track_expiry = 2678400
        self.expiry_check_interval = 21600
        self.votes_to_approve = 2
        self.self_voting_enabled = False


database_name = "PlaylistManagement.db"

configuration = TestConfiguration()
#configuration = ReleaseConfiguration()
