#! /usr/bin/env python
# -*- coding: utf-8 -*-

import Config
import PlaylistManagement

from sqlite3worker import Sqlite3Worker
from dateutil import parser


class DatabaseManager:
    def __init__(self):
        self.db_manager = Sqlite3Worker(Config.database_name)

    def clear_db(self):
        self.db_manager.execute("DELETE FROM playlist")
        self.db_manager.execute("DELETE FROM playlist_candidates")
        self.db_manager.execute("DELETE FROM rated_by")

    def clear_playlist(self):
        self.db_manager.execute("DELETE FROM playlist")

    def clear_candidates(self):
        self.db_manager.execute("DELETE FROM playlist_candidates")
        self.db_manager.execute("DELETE FROM rated_by")

    def save_chat_id(self, chat_id):
        self.db_manager.execute("UPDATE chat_id SET chat_id = ?", (chat_id,))

    def add_playlist_candidate(self, playlist_candidate):
        message_id = playlist_candidate.message_id
        proposing_user_id = playlist_candidate.proposing_user_id
        track_id = playlist_candidate.track_id

        self.db_manager.execute(
            "INSERT INTO playlist_candidates VALUES(?,?,?)", (message_id, proposing_user_id, track_id))

    def rate_playlist_candidate(self, playlist_candidate, user_id):
        track_id = playlist_candidate.track_id

        self.db_manager.execute(
            "INSERT INTO rated_by VALUES(?,?)", (track_id, user_id))

    def add_candidate_to_playlist(self, playlist_track):
        track_id = playlist_track.track_id
        add_date = playlist_track.add_date

        self.db_manager.execute(
            "DELETE FROM playlist_candidates WHERE track_id = ?", (track_id,))
        self.db_manager.execute(
            "DELETE FROM rated_by WHERE track_id = ?", (track_id,))
        self.db_manager.execute(
            "INSERT INTO playlist VALUES(?,?)", (track_id, add_date))

    def remove_expired_track(self, playlist_track):
        track_id = playlist_track.track_id

        self.db_manager.execute(
            "DELETE FROM playlist WHERE track_id = ?", (track_id,))

    def load_rated_by(self, track_id):

        query_result = self.db_manager.execute(
            "SELECT * FROM rated_by WHERE track_id = ?", (track_id,))

        rated_by = list()
        for row in query_result:
            rated_by.append(row[1])

        return rated_by

    def load_playlist_candidate(self, track_id):

        query_result = self.db_manager.execute(
            "SELECT * FROM playlist_candidates WHERE track_id = ?", (track_id,))

        message_id = query_result[0][0]
        proposing_user_id = query_result[0][1]

        rated_by = self.load_rated_by(track_id)

        playlist_candidate = PlaylistManagement.PlaylistCandidate(
            message_id, proposing_user_id, track_id)
        playlist_candidate.rated_by = rated_by
        return playlist_candidate

    def load_playlist_candidates(self):

        query_result = self.db_manager.execute(
            "SELECT track_id FROM playlist_candidates")

        playlist_candidates = dict()
        for row in query_result:
            playlist_candidate = self.load_playlist_candidate(row[0])
            playlist_candidates[playlist_candidate.message_id] = playlist_candidate

        return playlist_candidates

    def load_playlist_track(self, track_id):
        query_result = self.db_manager.execute(
            "SELECT expiry FROM playlist WHERE track_id = ?", (track_id,))

        add_date_str = query_result[0][0]
        add_date = parser.parse(add_date_str)

        return PlaylistManagement.PlaylistTrack(track_id, add_date)

    def load_playlist(self):
        query_result = self.db_manager.execute("SELECT track_id FROM playlist")

        playlist = dict()
        for row in query_result:
            playlist_track = self.load_playlist_track(row[0])
            playlist[playlist_track.track_id] = playlist_track

        return playlist

    def load_chat_id(self):
        query_result = self.db_manager.execute("SELECT * FROM chat_id")

        return query_result[0][0]
