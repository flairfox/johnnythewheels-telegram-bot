#! /usr/bin/env python
# -*- coding: utf-8 -*-

import Config
import DatabaseManagement

from datetime import datetime, timedelta


class PlaylistTrack():
    def __init__(self, track_id, add_date):
        self.track_id = track_id
        self.add_date = add_date


class PlaylistCandidate():
    def __init__(self, message_id, proposing_user_id, track_id):
        self.message_id = message_id
        self.proposing_user_id = proposing_user_id
        self.track_id = track_id
        self.rated_by = list()


class PlaylistManager():

    def __init__(self):
        self.database_manager = DatabaseManagement.DatabaseManager()

        self.playlist_candidates = self.database_manager.load_playlist_candidates()
        self.playlist = self.database_manager.load_playlist()
        self.chat_id = self.database_manager.load_chat_id()

    def clear(self):
        self.playlist.clear()
        self.playlist_candidates.clear()
        self.database_manager.clear_db()

    def clear_playlist(self):
        self.playlist.clear()
        self.database_manager.clear_playlist()

    def clear_candidates(self):
        self.playlist_candidates.clear()
        self.database_manager.clear_candidates()

    def set_chat_id(self, chat_id):
        self.chat_id = chat_id
        self.database_manager.save_chat_id(chat_id)

    def find_message_id(self, track_id):
        for playlist_candidate in self.playlist_candidates.values():
            if playlist_candidate.track_id == track_id:
                return playlist_candidate.message_id

    def is_already_in_candidates(self, track_id):
        for playlist_candidate in self.playlist_candidates.values():
            if playlist_candidate.track_id == track_id:
                return True

        return False

    def is_already_in_playlist(self, track_id):
        for playlist_track in self.playlist.values():
            if playlist_track.track_id == track_id:
                return True

        return False

    def add_playlist_candidate(self, message_id, proposing_user_id, track_id):
        for playlist_candidate in self.playlist_candidates.values():
            if playlist_candidate.track_id == track_id:
                return 'already_in_candidates'

        for playlist_track in self.playlist.values():
            if playlist_track.track_id == track_id:
                return 'already_in_playlist'

        new_playlist_candidate = PlaylistCandidate(
            message_id, proposing_user_id, track_id)
        self.playlist_candidates[message_id] = new_playlist_candidate
        self.database_manager.add_playlist_candidate(new_playlist_candidate)

    def rate_playlist_candidate(self, message_id, user_id):
        playlist_candidate = self.playlist_candidates.get(str(message_id))
        user_id = str(user_id)

        # Проверяем, не голосует ли пользователь за свой же трек
        if user_id == str(playlist_candidate.proposing_user_id):
            if Config.configuration.self_voting_enabled:
                pass
            else:
                return 'self_voting'
        # Проверяем нет ли повторного голосования
        if user_id in playlist_candidate.rated_by:
            return 'double_voting'

        playlist_candidate.rated_by.append(user_id)
        self.database_manager.rate_playlist_candidate(
            playlist_candidate, user_id)

        if len(playlist_candidate.rated_by) >= Config.configuration.votes_to_approve:
            track_id = playlist_candidate.track_id
            self.playlist_candidates.pop(str(message_id))
            add_date = datetime.now()

            playlist_track = PlaylistTrack(track_id, add_date)

            self.playlist[playlist_track.track_id] = playlist_track
            self.database_manager.add_candidate_to_playlist(playlist_track)

            return 'candidate_approved'
        else:
            self.playlist_candidates[message_id] = playlist_candidate
            return 'candidate_rated'

    def remove_expired_track(self, playlist_track):
        self.playlist.pop(playlist_track.track_id)
        self.database_manager.remove_expired_track(playlist_track)

        return 'track_removed'

    def _load_playlist_candidates(self):
        pass
