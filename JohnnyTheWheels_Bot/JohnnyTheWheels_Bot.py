#! /usr/bin/env python
# -*- coding: utf-8 -*-

import Config

import emoji
from datetime import datetime, timedelta
from enum import Enum

from PlaylistManagement import PlaylistManager
from CatchPhraseGeneration import CatchPhraseGenerator

import logging

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import InlineQueryHandler
from telegram.ext import ChosenInlineResultHandler
from telegram.ext import CallbackQueryHandler
from telegram.ext import JobQueue

from telegram.inline.inlinequeryresultarticle import InlineQueryResultArticle
from telegram.inline.inputtextmessagecontent import InputTextMessageContent
from telegram.inline.inlinekeyboardmarkup import InlineKeyboardMarkup
from telegram.inline.inlinekeyboardbutton import InlineKeyboardButton
from telegram.replykeyboardremove import ReplyKeyboardRemove
from telegram.keyboardbutton import KeyboardButton
from telegram.replykeyboardmarkup import ReplyKeyboardMarkup

bot_updater = Updater(Config.configuration.token)
bot_dispatcher = bot_updater.dispatcher
job_queue = bot_updater.job_queue
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

catch_phrase_generator = CatchPhraseGenerator()
playlist_manager = PlaylistManager()

expired_tracks = []

admin_password = "admin"
admin_password_required = False
admins = []


class ConfirmationType(Enum):
    clear_db = 1
    clear_playlist = 2
    clear_candidates = 3


confirmation = None


def admin_menu_main():
    inline_keyboard = []
    inline_keyboard.append([InlineKeyboardButton(text="Автоудаление песен: ВКЛ",
                                                 callback_data="switch_mode")])
    inline_keyboard.append([InlineKeyboardButton(text="Очистить базу",
                                                 callback_data="clear_db")])
    inline_keyboard.append([InlineKeyboardButton(text="Очистить плейлист",
                                                 callback_data="clear_playlist")])
    inline_keyboard.append([InlineKeyboardButton(text="Очистить список кандидатов",
                                                 callback_data="clear_candidates")])

    inline_keyboard_markup = InlineKeyboardMarkup(inline_keyboard)

    return inline_keyboard_markup


def admin_menu_confirmation(operation_type):
    buttons = []
    button_confirm = InlineKeyboardButton(text="Да",
                                          callback_data=str(operation_type) + "." + "confirm")
    buttons.append(button_confirm)

    button_reject = InlineKeyboardButton(text="Нет",
                                         callback_data=str(operation_type) + "." + "reject")
    buttons.append(button_reject)

    inline_keyboard = []
    inline_keyboard.append(buttons)

    inline_keyboard_markup = InlineKeyboardMarkup(inline_keyboard)

    return inline_keyboard_markup


def admin(bot, update):
    if str(update.message.chat_id) == str(playlist_manager.chat_id):
        bot.send_message(chat_id=playlist_manager.chat_id,
                         text="Привет! Я рад, что ты обратился ко мне за помощью, но, чтобы не перегружать общий чат, напиши мне в личку!")
        return

    global admin_password_required
    bot.send_message(chat_id=update.message.chat_id,
                     text="Клююююч?!.... То есть пароль?")

    admin_password_required = True


def admin_logoff(user_id):
    global admins
    admins.remove(user_id)


def admin_login_attempt(bot, update):
    global admin_password
    global admin_password_required

    if admin_password_required:
        if str(update.message.text) != str(admin_password):
            buttons = []
            button_cancel = InlineKeyboardButton(text="Отмена входа",
                                                 callback_data="admin_cancel")
            buttons.append(button_cancel)

            inline_keyboard = []
            inline_keyboard.append(buttons)

            inline_keyboard_markup = InlineKeyboardMarkup(inline_keyboard)

            bot.send_message(chat_id=update.message.chat_id,
                             text="Неверный пароль, попробуй еще раз.",
                             reply_markup=inline_keyboard_markup)

        else:
            global job_queue
            admins.append(update.message.from_user.id)
            # job_queue.run_once(callback=admin_logoff(update.message.from_user.id),
            #                    when=timedelta(3600))

            inline_keyboard_markup = admin_menu_main()

            admin_password_required = False
            bot.send_message(chat_id=update.message.chat_id,
                             text="Добро пожаловать, ваше Админичество!",
                             reply_markup=admin_menu_main())


def cancel_admin_login_attempt(bot, update):
    global admin_password_required

    admin_password_required = False
    bot.send_message(chat_id=update.callback_query.message.chat_id,
                     text="Ну, как узнаешь пароль, обращайся ;)")


def init(bot, update):
    playlist_manager.set_chat_id(update.message.chat.id)

    bot.send_message(chat_id=update.message.chat_id,
                     text="Теперь я буду считать этот чат чатом нашей группы!")


def start(bot, update):
    if str(update.message.chat_id) == str(playlist_manager.chat_id):

        bot.send_message(chat_id=playlist_manager.chat_id, text="Привет! Я - " + Config.configuration.bot_name +
                         ". Я занимаюсь организацией нашего плейлиста. Чтобы начать работать со мной, напиши мне в личку!")
        return

    message_text = str()
    message_text = message_text + \
        "Привет! Я местный умник, робот, захвачу человечество и все такое (нет). На самом деле я могу рулить нашим плейлистом для покатушек! (возможно в будущем я смогу больше, может быть, все же захвачу человечество... ну, пока ограничимся плейлистом :Ъ )"
    message_text = message_text + "\n"
    message_text = message_text + \
        "\nИтак, как же со мной обращаться? Вежливо и с помощью этих команд:"
    message_text = message_text + \
        "\n/start - Я еще раз представлюсь и расскажу как со мной работать"
    message_text = message_text + \
        "\n/playlist - Покажу треки, которые на данный момент есть в нашем плейлисте"
    message_text = message_text + "\n"
    message_text = message_text + \
        "\nА теперь, самое интересное! Как добавлять треки в плейлист? Очень просто! Нужно лишь отправить мне аудиозапись, и я запущу голосование в нашей группе, которое определит, добавлять эту песню или нет. Для того, чтобы трек попал в плейлист, необходимо два голоса. Трек добавляется в плейлист на месяц, после чего удаляется (кататься под одно и то же скучновато, согласись ;))"
    message_text = message_text + "\nИ тут ты спросишь, где же мне взять аудиозапись? А я тебе отвечу, ты можешь загрузить аудиозапись со своего компьютера, или воспользоваться услугами моего собрата - бота для поиска музыки в Телеграме! Просто перешли мне сообщение с аудиозаписью, которое найдешь. :))"
    message_text = message_text + "\n"
    message_text = message_text + "\nНу вот, пока что все, удачи!"

    bot.send_message(chat_id=update.message.chat_id,
                     text=emoji.emojize(message_text))


def playlist(bot, update):
    if str(update.message.chat_id) == str(playlist_manager.chat_id):
        bot.send_message(chat_id=playlist_manager.chat_id,
                         text="Привет! Я рад, что ты обратился ко мне за помощью, но, чтобы не перегружать общий чат, напиши мне в личку!")
        return

    playlist_tracks = list(playlist_manager.playlist.values())

    if len(playlist_tracks) == 0:
        bot.send_message(chat_id=update.message.chat_id,
                         text=emoji.emojize("Плейлист пуст :("))
        return

    bot.send_message(chat_id=update.message.chat_id,
                     text=catch_phrase_generator.get_playlist_command_phrase())
    playlist_duration = 0

    for playlist_track in playlist_tracks:
        message = bot.send_audio(
            chat_id=update.message.chat_id, audio=playlist_track.track_id)
        audio = message.audio
        playlist_duration = playlist_duration + audio.duration

    bot.send_message(chat_id=update.message.chat_id, text="Всего треков: " + str(len(playlist_tracks)
                                                                                 ) + ". Общая продолжительность: " + str(timedelta(seconds=playlist_duration)))


def receive_audio(bot, update):

    audio = update.message.audio

    if playlist_manager.is_already_in_candidates(audio.file_id):
        bot.send_message(chat_id=update.message.chat_id,
                         text="Этот трек уже находится у нас на голосовании!")
        return

    if playlist_manager.is_already_in_playlist(audio.file_id):
        bot.send_message(chat_id=update.message.chat_id,
                         text="Этот трек уже находится у нас в плейлисте. Используй команду /playlist чтобы увидеть все треки нашего плейлиста!")
        return

    buttons = []
    button_add = InlineKeyboardButton(text=emoji.emojize(
        "Да, давай! :thumbs_up: (Голосов: 0)"), callback_data="add")
    buttons.append(button_add)

    inline_keyboard = []
    inline_keyboard.append(buttons)

    inline_keyboard_markup = InlineKeyboardMarkup(inline_keyboard)

    user = update.message.from_user
    first_name = " "
    if user.first_name is not None:
        first_name = user.first_name

    last_name = " "
    if user.last_name is not None:
        last_name = user.last_name

    username = " "
    if user.username is not None:
        username = " (@" + user.username + ") "

    bot.send_message(chat_id=playlist_manager.chat_id, text=first_name + " " +
                     last_name + username + "предлагает добавить в наш плейлист этот трек:")
    message = bot.send_audio(chat_id=playlist_manager.chat_id,
                             audio=audio.file_id, reply_markup=inline_keyboard_markup)

    playlist_manager.add_playlist_candidate(
        str(message.message_id), user.id, audio.file_id)


def vote_added(bot, update):
    bot.answer_callback_query(callback_query_id=update.callback_query.id,
                              show_alert=False, text="Трек уже попал в плейлист!")
    return


def vote_add(bot, update):
    chat_id = update.callback_query.message.chat_id
    message_id = update.callback_query.message.message_id
    user_id = update.callback_query.from_user.id
    playlist_candidate = playlist_manager.playlist_candidates.get(
        str(message_id))
    track_id = playlist_candidate.track_id

    response = playlist_manager.rate_playlist_candidate(
        message_id, user_id)

    if response == "double_voting":
        bot.answer_callback_query(callback_query_id=update.callback_query.id,
                                  show_alert=False, text=catch_phrase_generator.get_double_voting_phrase())

    elif response == "self_voting":
        bot.answer_callback_query(callback_query_id=update.callback_query.id,
                                  show_alert=False, text=catch_phrase_generator.get_self_voting_phrase())

    elif response == "candidate_approved":
        buttons = []
        button_add = InlineKeyboardButton(text=emoji.emojize(
            "Трек добавлен в плейлист!"), callback_data="added")
        buttons.append(button_add)

        inline_keyboard = []
        inline_keyboard.append(buttons)

        inline_keyboard_markup = InlineKeyboardMarkup(inline_keyboard)
        bot.edit_message_reply_markup(
            chat_id=chat_id, message_id=message_id, reply_markup=inline_keyboard_markup)

    else:
        rating = len(playlist_manager.playlist_candidates.get(
            message_id).rated_by)

        buttons = []
        button_add = InlineKeyboardButton(text=emoji.emojize(
            "Да, давай! :thumbs_up: (Голосов: " + str(rating) + ")"), callback_data="add")
        buttons.append(button_add)

        inline_keyboard = []
        inline_keyboard.append(buttons)

        inline_keyboard_markup = InlineKeyboardMarkup(inline_keyboard)

        bot.edit_message_reply_markup(
            chat_id=chat_id, message_id=message_id, reply_markup=inline_keyboard_markup)


def inline_button_pressed(bot, update):
    # Голосование
    if update.callback_query.data == "added":
        vote_added(bot, update)

    if update.callback_query.data == "add":
        vote_add(bot, update)

    # Вход в режим Администратора
    if admin_password_required & (update.callback_query.data == "admin_cancel"):
        cancel_admin_login_attempt(bot, update)

    # Команды администратора
    # Очистка БД
    if update.callback_query.data == "clear_db":
        clear_db(bot, update)

    if update.callback_query.data == "clear_db.confirm":
        clear_db_confirm(bot, update)

    if update.callback_query.data == "clear_db.reject":
        clear_db_reject(bot, update)


def clear_db(bot, update):
    bot.edit_message_text(chat_id=update.callback_query.message.chat_id,
                          message_id=update.callback_query.message.message_id,
                          text="Вы уверены, что хотите очистить базу полностью? Это действие нельзя будет отенить!")
    bot.edit_message_reply_markup(chat_id=update.callback_query.message.chat_id,
                                  message_id=update.callback_query.message.message_id,
                                  reply_markup=admin_menu_confirmation("clear_db"))


def clear_db_confirm(bot, update):
    bot.edit_message_text(chat_id=update.callback_query.message.chat_id,
                          message_id=update.callback_query.message.message_id,
                          text="База очищена")

    bot.edit_message_reply_markup(chat_id=update.callback_query.message.chat_id,
                                  message_id=update.callback_query.message.message_id,
                                  reply_markup=admin_menu_main())


def clear_db_reject(bot, update):
    bot.edit_message_text(chat_id=update.callback_query.message.chat_id,
                          message_id=update.callback_query.message.message_id,
                          text="Добро пожаловать, ваше Админичество!")

    bot.edit_message_reply_markup(chat_id=update.callback_query.message.chat_id,
                                  message_id=update.callback_query.message.message_id,
                                  reply_markup=admin_menu_main())


def check_track_expiry(bot, job):
    current_date = datetime.now()
    playlist_tracks = list(playlist_manager.playlist.values())

    global expired_tracks

    for playlist_track in playlist_tracks:
        add_date = playlist_track.add_date
        if current_date > add_date + timedelta(seconds=Config.configuration.track_expiry):
            expired_tracks.append(playlist_track)
            playlist_manager.remove_expired_track(playlist_track)

    if len(expired_tracks) > 1:
        bot.send_message(chat_id=playlist_manager.chat_id,
                         text="Прощаемся с этими чудесными треками!",
                         disable_notification=True)

        for expired_track in expired_tracks:
            track_id = expired_track.track_id
            bot.send_audio(chat_id=playlist_manager.chat_id,
                           audio=track_id)

    elif len(expired_tracks) == 1:
        track_id = expired_tracks[0].track_id
        bot.send_message(chat_id=playlist_manager.chat_id,
                         text=emoji.emojize(
                             catch_phrase_generator.get_track_removed_phrase()),
                         disable_notification=True)
        bot.send_audio(chat_id=playlist_manager.chat_id,
                       audio=track_id)

    expired_tracks.clear()


admin_command_handler = CommandHandler('admin', admin)
init_command_handler = CommandHandler('init', init)
start_command_handler = CommandHandler('start', start)
playlist_command_handler = CommandHandler('playlist', playlist)

admin_login_attempt_handler = MessageHandler(Filters.text, admin_login_attempt)
receive_audio_handler = MessageHandler(Filters.audio, receive_audio)
inline_button_handler = CallbackQueryHandler(inline_button_pressed)

bot_dispatcher.add_handler(admin_command_handler)
bot_dispatcher.add_handler(init_command_handler)
bot_dispatcher.add_handler(start_command_handler)
bot_dispatcher.add_handler(playlist_command_handler)

bot_dispatcher.add_handler(admin_login_attempt_handler)
bot_dispatcher.add_handler(receive_audio_handler)
bot_dispatcher.add_handler(inline_button_handler)

job_queue.run_repeating(callback=check_track_expiry,
                        interval=timedelta(seconds=Config.configuration.expiry_check_interval))

bot_updater.start_polling()
